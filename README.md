# Lee's Simple Book Database

![Program Screenshot](./lsbd_console.jpg)

A simple book database program to store information about books. This program serves more as a proof-of-concept to see if I can create a fully functional program that does not depend on creating a program clone like I did when I worked on the [ReactOS](https://www.reactos.org/) project in the past. The final goal is to create a simple information storage program to keep track of a large list of books.

## Building the Program

I am currently developing this program on [Kubuntu](https://kubuntu.org/) 22.04 LTS with the tools and SDK that comes default with that version. The build system uses [Meson](https://mesonbuild.com/), so it should be straightforward to use. Keep in mind that this has not been tested or built on other platforms.

### VALA Dependencies

* Glib
* Sqlite
* Readline
