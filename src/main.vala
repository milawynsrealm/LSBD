using Sqlite;

class HomeDatabase : GLib.Object
{
    private static bool appVer = false;
    public static string? dbLocation = null;

    private const GLib.OptionEntry[] options = {
        { "location", 'l', OptionFlags.NONE, OptionArg.FILENAME, ref dbLocation, "Connect to database on startup", "DIRECTORY" },
        { "version", 'v', OptionFlags.NONE, OptionArg.NONE, ref appVer, "Display the application version", null },
        { null }
    };

    private static void show_version_short()
    {
        /* If the user just asks for the version, just show this one
         * line since most unix-based programs expect this behavior */
        stdout.printf("Lee's Simple Book Database v0.02\n");
    }

    private static void show_version()
    {
        show_version_short();
        stdout.printf("Copyright (C) 2022 Lee Schroeder\n");
        stdout.printf("License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.\n");
        stdout.printf("This is free software: you are free to change and redistribute it.\n");
        stdout.printf("There is NO WARRANTY, to the extent permitted by law.\n");
    }

    /* Main entry point for the program */
    public static int main(string[] args)
    {
        DBaseMain dbMain = new DBaseMain();
        Interpreter iptMain = new Interpreter();
        string? queryScript = "";
        bool isRunning = true, showList = false;

        /* Get the command line information */
        try
        {
            var opt_context = new OptionContext("Collects information about books");
            opt_context.set_help_enabled (true);
            opt_context.add_main_entries (options, null);
            opt_context.parse (ref args);
        }
        catch(OptionError e)
        {
            printerr ("error: %s\n", e.message);
            printerr ("Run '%s --help' to see a full list of available command line options.\n", args[0]);
            return 1;
        };

        /* If the user wants to just see the version information */
        if (appVer)
        {
            show_version_short();
            return 0;
        }

         /* Show the header information */
        show_version();

        /* Try to open the database for use */
        if (!dbMain.Open(dbLocation))
            return -1;

        /* Let the user know about the help system */
        stdout.printf("\nType 'help' for more information.\n");

        /* The main input loop */
        do
        {
            /* Run the main interpreter */
            isRunning = iptMain.Run(out queryScript, out showList);

            //stdout.printf("%s\n", queryScript); //DEBUG

            if (showList)
                dbMain.GetList();

            /* As long as there is a query to process... */
            if ((queryScript != null) || (queryScript != ""))
            {
                /* Inject SQL script for manipulating the database */
                if (!dbMain.QueryCode(queryScript))
                    return 0;
            }
        }
        while (isRunning);

        /* Return success */
        return 0;
    }
}
