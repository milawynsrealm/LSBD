using Sqlite;

public class Interpreter : GLib.Object
{
    private string? userInput = "";

    private void ShowHelp()
    {
        /* Show a list of available options */
        stdout.printf("add    - Creates a new entry.\n");
        stdout.printf("create - Creates a new entry.\n");
        stdout.printf("modify - Modify an existing entry.\n");
        stdout.printf("delete - Remove an existing entry.\n");
        stdout.printf("list   - List all existing entries.\n");
        stdout.printf("help   - Show this help information.\n");
        stdout.printf("exit   - Exit the application.\n\n");
    }

    private bool CreateEntry(out string? output)
    {
        string? ceTitle, ceAuthor, ceIsbn, cePublish, ceYear;
        bool inLoop = true;

        /* Assign default values */
        ceTitle = "";
        ceAuthor = "";
        ceIsbn = "";
        cePublish = "";
        ceYear = "";

        /* Default output if nothing valid is added */
        output = "";

        do {
            /* Get the title of the book */
            ceTitle = Readline.readline("Title> ");
            if (ceTitle != null && ceTitle != "")
            {
                Readline.History.add (ceTitle);
                inLoop = false;
            }
        } while(inLoop);

        /* Reset value */
        inLoop = true;

        do {
            /* Get the author of the book */
            ceAuthor = Readline.readline("Author> ");
            if (ceAuthor != null && ceAuthor != "")
            {
                Readline.History.add (ceAuthor);
                inLoop = false;
            }
        } while(inLoop);

        /* Reset value */
        inLoop = true;

        do {
            /* Get the ISBN of the book */
            ceIsbn = Readline.readline("ISBN> ");
            if (ceIsbn != null && ceIsbn != "")
            {
                Readline.History.add (ceIsbn);
                inLoop = false;
            }
        } while(inLoop);

        /* Reset value */
        inLoop = true;

        do {
            /* Get the publisher of the book */
            cePublish = Readline.readline("Publisher> ");
            if (cePublish != null && cePublish != "")
            {
                Readline.History.add (cePublish);
                inLoop = false;
            }
        } while(inLoop);

        /* Reset value */
        inLoop = true;

        do {
            /* Get the year of release of the book */
            ceYear = Readline.readline("Year> ");
            if (ceYear != null && ceYear != "")
            {
                Readline.History.add (ceYear);
                inLoop = false;
            }
        } while(inLoop);

        /* Check to make sure the user enterd in everything */
        if ((ceTitle == null && ceTitle == "") &&
            (ceAuthor == null && ceAuthor == "") &&
            (ceIsbn == null && ceIsbn == "") &&
            (cePublish == null && cePublish == "") &&
            (ceYear == null && ceYear == "")
        )
        {
            /* All entries must be filled before it can be added */
            printerr("ERROR: Not all fields were filled. Entry cancelled!\n");
            return false;
        }

        /* Stitches the final output together before sending it back */
        output = output.concat("INSERT INTO books (title, author, isbn, publisher, year) VALUES ('",
                               ceTitle, "', '",
                               ceAuthor, "', '",
                               ceIsbn, "', '",
                               cePublish, "', '",
                               ceYear, "');");

        //stdout.printf("%s\n", output); //DEBUG

        /* Let the user know the entry was created */
        stdout.printf("Entry created!\n\n");

        /* Entry was created sucessfully */
        return true;
    }

    private bool ModifyEntry(out string? output)
    {
        string? ModEntry;
        string? ceTitle, ceAuthor, ceIsbn, cePublish, ceYear;
        bool isChanged = false, isFirst = false;
        int indexVal = 0;

        /* Default output */
        output = "";

        output = output.concat("UPDATE books SET ");

        /* Lets the user know what to enter */
        stdout.printf("This allows you to modify existing entires. After providing the index to\n");
        stdout.printf("alter, provide the alternative data. To leave unchanged, leave it blank.\n\n");

        /* Get the index number */
        ModEntry = Readline.readline("Index #> ");
        if (ModEntry != null && ModEntry != "")
        {
            /* Make sure the index number is valid */
            indexVal = int.parse(ModEntry);
            if (indexVal <= 0)
            {
                printerr("ERROR: Index value must be 1 or greater.\n");
                return true;
            }

            /* Only add to history if the number is valid */
            Readline.History.add (ModEntry);
        }

        /* Get the title of the book */
        ceTitle = Readline.readline("Title> ");
        if (ceTitle != null && ceTitle != "")
        {
            Readline.History.add (ceTitle);
            output = output.concat("title = '", ceTitle, "'");
            isChanged = true;
            isFirst = true;
        }

        /* Get the author of the book */
        ceAuthor = Readline.readline("Author> ");
        if (ceAuthor != null && ceAuthor != "")
        {
            /* Add a comma only if there was an item before it */
            if (isFirst)
                output = output.concat(", ");
            else
                isFirst = true;

            Readline.History.add (ceAuthor);
            output = output.concat("author = '", ceAuthor, "'");
            isChanged = true;
        }

        /* Get the ISBN of the book */
        ceIsbn = Readline.readline("ISBN> ");
        if (ceIsbn != null && ceIsbn != "")
        {
            /* Add a comma only if there was an item before it */
            if (isFirst)
                output = output.concat(", ");
            else
                isFirst = true;

            Readline.History.add (ceIsbn);
            output = output.concat("isbn = '", ceIsbn, "'");
            isChanged = true;
        }

        /* Get the publisher of the book */
        cePublish = Readline.readline("Publisher> ");
        if (cePublish != null && cePublish != "")
        {
            /* Add a comma only if there was an item before it */
            if (isFirst)
                output = output.concat(", ");
            else
                isFirst = true;

            Readline.History.add (cePublish);
            output = output.concat("publisher = '", cePublish, "'");
            isChanged = true;
        }

        /* Get the year of release of the book */
        ceYear = Readline.readline("Year> ");
        if (ceYear != null && ceYear != "")
        {
            /* Add a comma only if there was an item before it */
            if (isFirst)
                output = output.concat(", ");
            else
                isFirst = true;

            Readline.History.add (ceYear);
            output = output.concat("year = '", ceYear, "'");
            isChanged = true;
        }

        /* If nothing is changed, then don't send anything to the
         * SQL query */
        if (!isChanged)
        {
            stdout.printf("Nothing to change!\n\n");
            return true;
        }

        /* Add the tail to the SQL query */
        output = output.concat(" WHERE book_id = ", indexVal.to_string(), ";");

        //stdout.printf("%s\n", output); //DEBUG

        /* Done here */
        return true;
    }

    private bool DeleteEntry(out string? output)
    {
        string? delEntry;
        int indexVal = 0;

        /* Default output */
        output = "";

        /* Let the user know what to enter */
        stdout.printf("Entry deletion is done by selecting the ID number (1, 2, 3, etc.). To ensure\n");
        stdout.printf("you are deleting the correct entry, please get the index nummber first.\n\n");

        /* Get the index number */
        delEntry = Readline.readline("Index #> ");
        if (delEntry != null && delEntry != "")
            Readline.History.add (delEntry);

        /* Easiest way to check for valid index value. Anything the parse command cannot process
         * properly will default to 0, which is an invalid index value. Lets the program know there's
         * an invalid value. */
        indexVal = int.parse(delEntry);
        if (indexVal <= 0)
        {
            printerr("ERROR: Index value must be 1 or greater.\n");
            return true;
        }

        /* Make an attempt to delete an entry */
        output = output.concat("DELETE FROM books WHERE book_id = ", indexVal.to_string(), ";");

        /* Let the user know the entry was created */
        stdout.printf("Entry removed!\n\n");

        return true;
    }

    public bool Run(out string? output, out bool showList)
    {
        /* Reset the value after each use */
        showList = false;

        /* Default output */
        output = "";

        /* Get input from the user */
        userInput = Readline.readline("> ");
        if (userInput != null && userInput != "")
            Readline.History.add (userInput);

        /* If the user wants to exit the program */
        if (userInput == "exit")
            return false;

        /* Determine what the user is asking for */
        if ((userInput == "create") || (userInput == "add"))
            return CreateEntry(out output);
        else if (userInput == "modify")
            return ModifyEntry(out output);
        else if (userInput == "delete")
            return DeleteEntry(out output);
        else if (userInput == "list")
        {
            showList = true;
            return true;
        }
        else if (userInput == "help")
            ShowHelp();
        else
        {
            /* Command not found, so let the user know */
            printerr("ERROR: Command '%s' not found! Type 'help' for available commands.\n", userInput);
        }

        /* All is well, please continue */
        return true;
    }
}
