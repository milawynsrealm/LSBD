using Sqlite;

public class DBaseMain : GLib.Object
{
    /* Database container */
    Database dbMain;

    public bool QueryCode(string? input)
    {
        string errMsg;

        /* If no input is provided, do nothing */
        if (input == null)
            return true;

        /* Process the SQL query code */
        if (dbMain.exec(input, null, out errMsg) != Sqlite.OK)
        {
            stdout.printf("ERROR: %s\n", errMsg);
            return false;
        }

        /* Everything ran smoothly */
        return true;
    }

    public void GetList()
    {
        string? listQuery = "SELECT * FROM books";
        int retVal = 0;
        string errmsg;
        string[] res;
        int nrows, ncols;

        /* Get the contents of the table */
        retVal = dbMain.get_table(listQuery, out res, out nrows, out ncols, out errmsg);
        if (retVal != Sqlite.OK)
        {
            printerr("ERROR: Unable to read the table contents.\n");
            return;
        }

        /* List the contents of the book list */
        int max = nrows * ncols + ncols;
        for(int i = 0; i < max; i = i + ncols)
        {
            for (int x = 0; x < ncols; x++)
            {
                //TODO: Cleanup the header?
                stdout.printf("%s", res[i + x]);
                stdout.printf("\t");
            }
            stdout.printf("\n");
        }
    }

    private bool CreateFile()
    {
        /* When the file is created, it is completely blank at
         * first. So we fill it up with the table we need to use
         * later on. */
        return QueryCode("""
            CREATE TABLE IF NOT EXISTS books (
                book_id INTEGER PRIMARY KEY,
                title TEXT NOT NULL,
                author TEXT NOT NULL,
                isbn TEXT NOT NULL,
                publisher TEXT NOT NULL,
                year YEAR NOT NULL
            );""");
    }

    public bool Open(string? filename)
    {
        /* If no database is provided, use the default one */
        if (filename == null)
        {
            //TODO: Put file into home directory
            filename = "./main.db";
        }

        /* Try to open the database, or the custom one if provided */
        if (Database.open(filename, out dbMain) != Sqlite.OK)
        {
            printerr ("ERROR: Unable to open: '%s' (%d - %s)\n", filename, dbMain.errcode(), dbMain.errmsg());
            return false;
        }

        /* If the database is being created for the first time, let the user know and continue */
        if (this.CreateFile())
            return true;

        /* Only get here if the tables don't get created for some reason */
        return false;
    }
}
